import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import DetialSection from './DetialSection';
import axios from 'axios';

export default class UserDetailScreen extends Component {

  constructor(){
    super();
    this.state = {
      userList: []
    };
  }

  componentDidMount() {
    axios({
      method: 'GET',
      url: "http://35.240.214.157:8080/api/bank-accounts",
      headers: { 
        Authorization: `Bearer ${window.localStorage.getItem('token')}`
      },
    }).then((response) => {
      if (response.status >= 400) {
        console.log("error");
        throw new Error("Server error: " + response.status);
      }
      return response.data;
    })
    .then((json) => {
      console.log(json);
      this.setState({
        userList: json
      })
    })
    .catch((exc) => {
      console.log(exc);
    });
  }
  // Properties used by this component:
  // appActions, deviceInfo

  render() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    if (this.props.transitionId && this.props.transitionId.length > 0 && this.props.atTopOfScreenStack && this.props.transitionForward) {
      baseStyle.animation = '0.25s ease-in-out '+this.props.transitionId;
    }
    if ( !this.props.atTopOfScreenStack) {
      layoutFlowStyle.height = '100vh';
      layoutFlowStyle.overflow = 'hidden';
    }
    
    const style_rectangle = {
        background: 'rgba(255, 255, 255, 1.000)',
        pointerEvents: 'none',
     };
    const style_comp_outer = {
        // backgroundColor: '#192b7d',
     };
    
    return (
      <div className="AppScreen UserDetailScreen" style={baseStyle}>
        <div className="background">
          <div className='containerMinHeight elRectangle' style={style_rectangle} />
        </div>
        <div className="layoutFlow" style={layoutFlowStyle}>
          <div className='hasNestedComps elHeader'>
            <div>
              <Header visualStateIndex={1} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>
          
          </div>
          
          <div className='hasNestedComps elComp' style={style_comp_outer}>
            <div>
              {
                this.state.userList.map((eachObject)=>(
                  <DetialSection details={eachObject} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
                ))
              }
            </div>
          
          </div>
          
        </div>
      </div>
    )
  }
  

}
