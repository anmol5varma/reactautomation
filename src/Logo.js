import React, { Component } from 'react';
import './App.css';


export default class Logo extends Component {

  // This component doesn't use any properties

  render() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_deutscheBank = {
        fontSize: 23.5,
        color: '#00285f',
        textAlign: 'left',
        pointerEvents: 'none',
     };
    
    return (
      <div className="Logo" style={baseStyle}>
        <div className="compContent">
          <div className='font-arialUnicodeMS  elDeutscheBank' style={style_deutscheBank}>
            <div>{this.props.locStrings.comp2_deutschebank_148910}</div>
          </div>
        </div>
      </div>
    )
  }
  

}
