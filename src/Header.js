import React, { Component } from 'react';
import './App.css';


export default class Header extends Component {

  // Properties used by this component:
  // visualStateIndex

  // --- Functions for component state index 0 (1 of 2) --- 
  
  renderState0() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangle2 = {
        background: 'rgba(0, 40, 95, 1.000)',
        border: '0.4px solid rgba(150, 150, 150, 0.4000)',
        pointerEvents: 'none',
     };
    const style_textBlock = {
        fontSize: 21.5,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        color: 'white',
        textAlign: 'left',
        pointerEvents: 'none',
     };
    // eslint-disable-next-line no-unused-vars
    const style_textBlock_inner = {
        textAlign: 'left',
     };
    
    return (
      <div className="Header" style={baseStyle}>
        <div className="compContent">
          <div className='containerMinHeight state0_elRectangle2' style={style_rectangle2} />
          <div className='containerMinHeight state0_elTextBlock' style={style_textBlock}>
            <div className="bottomAlignedContent">{this.props.locStrings.comp1_textblock_195426}</div>
          </div>
        </div>
      </div>
    )
  }
  
  // --- Functions for component state index 1 (2 of 2) --- 
  
  onClick_state1_elRectangle = (ev) => {
    window.localStorage.removeItem('token');
    this.props.appActions.goToScreen('login', { transitionId: 'fadeIn' });
  }
  
  
  renderState1() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangleCopy = {
        background: 'rgba(0, 40, 95, 1.000)',
        border: '0.4px solid rgba(150, 150, 150, 0.4000)',
        pointerEvents: 'none',
     };
    const style_textBlockCopy = {
        fontSize: 21.5,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        color: 'white',
        textAlign: 'left',
        pointerEvents: 'none',
     };
    // eslint-disable-next-line no-unused-vars
    const style_textBlockCopy_inner = {
        textAlign: 'left',
     };
    const style_rectangle = {
        background: 'rgba(255, 255, 255, 1.000)',
        cursor: 'pointer',
     };
    const style_text = {
        fontSize: 17.2,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        color: 'rgba(0, 0, 0, 0.8500)',
        textAlign: 'center',
        pointerEvents: 'none',
     };
    
    return (
      <div className="Header" style={baseStyle}>
        <div className="compContent">
          <div className='containerMinHeight state1_elRectangleCopy' style={style_rectangleCopy} />
          <div className='containerMinHeight state1_elTextBlockCopy' style={style_textBlockCopy}>
            <div className="bottomAlignedContent">{this.props.locStrings.header_textblock_595092}</div>
          </div>
          <div className='containerMinHeight state1_elRectangle' style={style_rectangle} onClick={this.onClick_state1_elRectangle}  />
          <div className='state1_elText' style={style_text}>
            <div>{this.props.locStrings.header_text_766811}</div>
          </div>
        </div>
      </div>
    )
  }
  
  
  render() {
    switch (parseInt((this.state && this.state.visualStateIndexOverride !== undefined) ? this.state.visualStateIndexOverride : this.props.visualStateIndex, 10)) {
      default:
      case 0:
        return this.renderState0();
      case 1:
        return this.renderState1();
    }
  }
  

}
