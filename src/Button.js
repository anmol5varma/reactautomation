import React, { Component } from 'react';
import './App.css';
import img_state0_elRectangle4 from './images/Button_state0_elRectangle4.png';
import img_state1_elRectangleCopy from './images/Button_state1_elRectangleCopy.png';
import img_state2_elRectangleCopy2 from './images/Button_state2_elRectangleCopy2.png';


export default class Button extends Component {

  // Properties used by this component:
  // visualStateIndex

  // --- Functions for component state index 0 (1 of 3) --- 
  
  renderState0() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangle4 = {
        backgroundImage: 'url('+img_state0_elRectangle4+')',
        backgroundSize: '100% 100%',
        pointerEvents: 'none',
     };
    const style_textBlock = {
        fontSize: 19.4,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        color: 'white',
        textAlign: 'center',
        pointerEvents: 'none',
     };
    
    return (
      <div className="Button" style={baseStyle}>
        <div className="compContent">
          <div className='state0_elRectangle4' style={style_rectangle4} />
          <div className='state0_elTextBlock' style={style_textBlock}>
            <div>{this.props.locStrings.comp1_textblock_996845}</div>
          </div>
        </div>
      </div>
    )
  }
  
  // --- Functions for component state index 1 (2 of 3) --- 
  
  renderState1() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangleCopy = {
        backgroundImage: 'url('+img_state1_elRectangleCopy+')',
        backgroundSize: '100% 100%',
        pointerEvents: 'none',
     };
    const style_textBlockCopy = {
        fontSize: 17.2,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        color: 'white',
        textAlign: 'center',
        pointerEvents: 'none',
     };
    
    return (
      <div className="Button" style={baseStyle}>
        <div className="compContent">
          <div className='state1_elRectangleCopy' style={style_rectangleCopy} />
          <div className='state1_elTextBlockCopy' style={style_textBlockCopy}>
            <div>{this.props.locStrings.comp1_textblock_529453}</div>
          </div>
        </div>
      </div>
    )
  }
  
  // --- Functions for component state index 2 (3 of 3) --- 
  
  renderState2() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangleCopy2 = {
        backgroundImage: 'url('+img_state2_elRectangleCopy2+')',
        backgroundSize: '100% 100%',
        pointerEvents: 'none',
     };
    const style_textBlockCopy2 = {
        fontSize: 17.2,
        fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", sans-serif',
        color: 'white',
        textAlign: 'center',
        pointerEvents: 'none',
     };
    
    return (
      <div className="Button" style={baseStyle}>
        <div className="compContent">
          <div className='state2_elRectangleCopy2' style={style_rectangleCopy2} />
          <div className='state2_elTextBlockCopy2' style={style_textBlockCopy2}>
            <div>{this.props.locStrings.comp1_textblock_520386}</div>
          </div>
        </div>
      </div>
    )
  }
  
  
  render() {
    switch (parseInt((this.state && this.state.visualStateIndexOverride !== undefined) ? this.state.visualStateIndexOverride : this.props.visualStateIndex, 10)) {
      default:
      case 0:
        return this.renderState0();
      case 1:
        return this.renderState1();
      case 2:
        return this.renderState2();
    }
  }
  

}
