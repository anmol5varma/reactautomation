import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import Logo from './Logo';
import InputField from './InputField';
import Button from './Button';
import axios from 'axios';


export default class SignupScreen extends Component {

  // Properties used by this component:
  // appActions, deviceInfo
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      fname: "",
      lname: "",
      email: "",
      phone: "",
      address: "",
      postalCode: "",
      panNumber: ""
    }
  }

  textInputChanged = (field, event) => {
    switch(field){
      case 'username': this.setState({ 'username': event.target.value }); break;
      case 'password': this.setState({ 'password': event.target.value }); break;
      case 'fname': this.setState({ 'fname': event.target.value }); break;
      case 'lname': this.setState({ 'lname': event.target.value }); break;
      case 'email': this.setState({ 'email': event.target.value }); break;
      case 'phone': this.setState({ 'phone': event.target.value }); break;
      case 'address': this.setState({ 'address': event.target.value }); break;
      case 'postalCode': this.setState({ 'postalCode': event.target.value }); break;
      case 'panNumber': this.setState({ 'panNumber': event.target.value }); break;
      default: break;
    }
    this.setState({ field: event.target.value });
  }

  onClick_elComp2 = (ev) => {
    console.log({
      username: this.state.username,
      password: this.state.password,
      firstName: this.state.fname,
      lastName: this.state.lname,
      email: this.state.email,
      phone: this.state.phone,
      address: this.state.address,
      postalCode: this.state.postalCode,
      panNumber: this.state.panNumber
    });
    axios.post('http://35.240.214.157:8080/api/authenticate', {
      username: 'admin',
      password: 'admin'
    }).then((response) => {
      return axios({
        method: 'POST',
        url: "http://35.240.214.157:8080/api/bank-accounts",
        headers: {
          Authorization: `Bearer ${response.data.id_token}`
        },
        data: {
          username: this.state.username,
          password: this.state.password,
          firstName: this.state.fname,
          lastName: this.state.lname,
          email: this.state.email,
          phone: this.state.phone,
          address: this.state.address,
          postalCode: this.state.postalCode,
          panNumber: this.state.panNumber
        }
      }).then((response) => {
        if (response.status >= 400) {
          console.log("error");
          throw new Error("Server error: " + response.status);
        }
        alert('Signup complete')
        this.props.appActions.goToScreen('login', { transitionId: 'fadeIn' });
      })
    }).catch((error) => {
      alert('Something went wrong. Try again after sometime');
    });
  }


  render() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    if (this.props.transitionId && this.props.transitionId.length > 0 && this.props.atTopOfScreenStack && this.props.transitionForward) {
      baseStyle.animation = '0.25s ease-in-out ' + this.props.transitionId;
    }
    if (!this.props.atTopOfScreenStack) {
      layoutFlowStyle.height = '100vh';
      layoutFlowStyle.overflow = 'hidden';
    }

    const style_background = {
      width: '100%',
      height: '100%',
    };
    const style_background_outer = {
      backgroundColor: '#f6f6f6',
      pointerEvents: 'none',
    };
    const style_username = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_username_outer = {
      pointerEvents: 'none',
    };
    const style_password = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_password_outer = {
      pointerEvents: 'none',
    };
    const style_lastName = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_lastName_outer = {
      pointerEvents: 'none',
    };
    const style_lastName2 = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_lastName2_outer = {
      pointerEvents: 'none',
    };
    const style_address = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_address_outer = {
      pointerEvents: 'none',
    };
    const style_email = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_email_outer = {
      pointerEvents: 'none',
    };
    const style_contact = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_contact_outer = {
      pointerEvents: 'none',
    };
    const style_postal = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_postal_outer = {
      pointerEvents: 'none',
    };
    const style_pan = {
      fontSize: 17.2,
      color: '#00285f',
      textAlign: 'left',
    };
    const style_pan_outer = {
      pointerEvents: 'none',
    };
    const style_comp2_outer = {
      cursor: 'pointer',
    };

    return (
      <div className="AppScreen SignupScreen" style={baseStyle}>
        <div className="background">
          <div className='appBg containerMinHeight elBackground' style={style_background_outer}>
            <div style={style_background} />

          </div>

        </div>
        <div className="layoutFlow" style={layoutFlowStyle}>
          <div className='hasNestedComps elHeader'>
            <div>
              <Header appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>

          </div>

          <div className='hasNestedComps elComp22'>
            <div>
              <Logo appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>

          </div>

          <div className='font-arialUnicodeMS  elUsername' style={style_username_outer}>
            <div style={style_username}>
              <div>{this.props.locStrings.n22_username_633195}</div>
            </div>

          </div>

          <div className='hasNestedComps elInputField7'>
            <div>
              <InputField appActions={this.props.appActions} defaultVal={this.state.username} setValue={(ev) => this.textInputChanged('username', ev)} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>

          </div>

          <div className='font-arialUnicodeMS  elPassword' style={style_password_outer}>
            <div style={style_password}>
              <div>{this.props.locStrings.n22_password_578283}</div>
            </div>

          </div>

          <div className='hasNestedComps elInputField8'>
            <div>
              <InputField visualStateIndex={2} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.password} setValue={(ev) => this.textInputChanged('password', ev)} />
            </div>

          </div>

          <div className='font-arialUnicodeMS  elLastName' style={style_lastName_outer}>
            <div style={style_lastName}>
              <div>{this.props.locStrings.n22_lastname_21226}</div>
            </div>

          </div>

          <div className='font-arialUnicodeMS  elLastName2' style={style_lastName2_outer}>
            <div style={style_lastName2}>
              <div>{this.props.locStrings.n22_lastname2_148058}</div>
            </div>

          </div>

          <div className='hasNestedComps elInputField5'>
            <div>
              <InputField visualStateIndex={1} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.fname} setValue={(ev) => this.textInputChanged('fname', ev)} />
            </div>

          </div>

          <div className='hasNestedComps elInputField6'>
            <div>
              <InputField visualStateIndex={1} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.lname} setValue={(ev) => this.textInputChanged('lname', ev)} />
            </div>

          </div>

          <div className='font-arialUnicodeMS  elAddress' style={style_address_outer}>
            <div style={style_address}>
              <div>{this.props.locStrings.n22_address_997623}</div>
            </div>

          </div>

          <div className='hasNestedComps elInputField'>
            <div>
              <InputField appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.address} setValue={(ev) => this.textInputChanged('address', ev)} />
            </div>

          </div>

          <div className='font-arialUnicodeMS  elEmail' style={style_email_outer}>
            <div style={style_email}>
              <div>{this.props.locStrings.n22_email_426332}</div>
            </div>

          </div>

          <div className='hasNestedComps elInputField3'>
            <div>
              <InputField visualStateIndex={3} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.email} setValue={(ev) => this.textInputChanged('email', ev)} />
            </div>

          </div>

          <div className='font-arialUnicodeMS  elContact' style={style_contact_outer}>
            <div style={style_contact}>
              <div>{this.props.locStrings.n22_contact_447557}</div>
            </div>

          </div>

          <div className='hasNestedComps elComp'>
            <div>
              <InputField visualStateIndex={4} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.contact} setValue={(ev) => this.textInputChanged('phone', ev)} />
            </div>

          </div>

          <div className='font-arialUnicodeMS  elPostal' style={style_postal_outer}>
            <div style={style_postal}>
              <div>{this.props.locStrings.n22_postal_798383}</div>
            </div>

          </div>

          <div className='font-arialUnicodeMS  elPan' style={style_pan_outer}>
            <div style={style_pan}>
              <div>{this.props.locStrings.n22_pan_371467}</div>
            </div>

          </div>

          <div className='hasNestedComps elInputField4'>
            <div>
              <InputField visualStateIndex={1} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.postalCode} setValue={(ev) => this.textInputChanged('postalCode', ev)} />
            </div>

          </div>

          <div className='hasNestedComps elInputField2'>
            <div>
              <InputField visualStateIndex={1} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} defaultVal={this.state.panNumber} setValue={(ev) => this.textInputChanged('panNumber', ev)} />
            </div>

          </div>

          <div className='hasNestedComps elComp2' style={style_comp2_outer}>
            <div onClick={this.onClick_elComp2} >
              <Button visualStateIndex={2} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>

          </div>

        </div>
      </div>
    )
  }


}
