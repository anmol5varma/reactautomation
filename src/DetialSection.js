import React, { Component } from 'react';
import './App.css';


export default class DetialSection extends Component {

  // This component doesn't use any properties

  render() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {
      borderBottom: '1px solid #0b295b',
    };
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};

    const style_rectangle = {
      background: 'rgba(254, 255, 254, 1.000)',
      pointerEvents: 'none',
    };
    const rowPadding = {
      paddingLeft: '5px'
    }

    return (
      <div className="DetialSection" style={baseStyle}>
        {/* <div className="compContent"> */}
          <div className='elRectangle' style={style_rectangle}>
            <table>
              <tbody>
                <tr>
                  <td>Name:</td>
                  <td style={rowPadding}>{this.props.details.firstName} {this.props.details.lastName}</td>
                </tr>
                <tr>
                  <td>Email:</td>
                  <td style={rowPadding}>{this.props.details.email}</td>
                </tr>
                <tr>
                  <td>Contact:</td>
                  <td style={rowPadding}>{this.props.details.phone}</td>
                </tr>
                <tr>
                  <td>PAN:</td>
                  <td style={rowPadding}>{this.props.details.panNumber}</td>
                </tr>
                <tr>
                  <td>Address:</td>
                  <td style={rowPadding}>{this.props.details.address}</td>
                </tr>
                <tr>
                  <td>Postal Code:</td>
                  <td style={rowPadding}>{this.props.details.postalCode}</td>
                </tr>
              </tbody>
            </table>
          </div>
        {/* </div> */}
      </div>
    )
  }


}
