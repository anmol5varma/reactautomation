import DataSheetBase from './DataSheetBase.js';

export default class DataSheet_localizationSheet extends DataSheetBase {

  constructor(id, updateCb) {
    super(id, updateCb);
    this.requestedKeyPath = "";  // this value can be specified in the React Studio data sheet UI
  }

  makeDefaultItems() {
    // eslint-disable-next-line no-unused-vars
    let key = 1;
    // eslint-disable-next-line no-unused-vars
    let item;
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxchecked_checklabel_894876";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtexterror_inputvalue_56413";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareadisabled_textareacontentd_728548";
    item['en'] = "Text area content (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutn_label_229991";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiocheckeddisabled_radiolabel_281690";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxcheckeddisabled_checklabel_987417";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallout_label_482258";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiounchecked_radiolabel_556769";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxuncheckeddisabled_checklabel_83628";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutse_label_743505";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutw_label_662895";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtext_inputvalue_918108";
    item['en'] = "Input value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareafocus_textareacontent_689735";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextplaceholder_placeholder_255863";
    item['en'] = "Placeholder";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiouncheckeddisabled_radiolabel_348643";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutne_label_426685";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimarydisabled_buttonlabel_525345";
    item['en'] = "Button Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareaerror_textareacontent_590799";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselect_selectoption_967340";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiochecked_radiolabel_1008826";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselectdisabled_selectoption_507137";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextdisabled_inputvaluedisable_954204";
    item['en'] = "Input value (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimary_ok_1036689";
    item['en'] = "OK";
    
    item = {};
    this.items.push(item);
    item['key'] = "formlegend_formelements_832446";
    item['en'] = "Form Elements";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondarydisabled_cancel_147304";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutsw_label_796695";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextfocus_inputvalue_645127";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextarea_textareacontent_843514";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutnw_label_776539";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallouts_label_391479";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloute_label_149639";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondary_cancel_257641";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxunchecked_checklabel_127940";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_mybank_918790";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_username_183957";
    item['en'] = "Username";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_password_842004";
    item['en'] = "Password";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_deutschebank_327033";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_login_1043936";
    item['en'] = "Login";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_createnewaccount_978672";
    item['en'] = "Create new Account";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_mybank_877897";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_username_477816";
    item['en'] = "Username";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_password_886191";
    item['en'] = "Password";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_lastname_743780";
    item['en'] = "First name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_lastname2_422553";
    item['en'] = "Last name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_address_61302";
    item['en'] = "Address";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_email_227739";
    item['en'] = "Email";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_contact_709120";
    item['en'] = "Phone number";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_postal_156871";
    item['en'] = "Postal Code";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_pan_556715";
    item['en'] = "PAN";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_deutschebank_271132";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n2_signup_124458";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_mybank_818377";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_fields_237810";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\n\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_value_567057";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_fields2_577258";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\n\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_value2_722426";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_fields3_754219";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\n\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_value3_970291";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_fields4_443047";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\n\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_value4_1029646";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n3_hianmol_950844";
    item['en'] = "Hi Anmol";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_createnewaccount_654436";
    item['en'] = "Create new Account";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_createnewaccount_317173";
    item['en'] = "Login\n";
    
    item = {};
    this.items.push(item);
    item['key'] = "n1_button_327339";
    item['en'] = "New button";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxchecked_checklabel_338057";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtexterror_inputvalue_768716";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareadisabled_textareacontentd_242587";
    item['en'] = "Text area content (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutn_label_22484";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiocheckeddisabled_radiolabel_478481";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxcheckeddisabled_checklabel_168152";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallout_label_620978";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiounchecked_radiolabel_597607";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxuncheckeddisabled_checklabel_887404";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutse_label_62947";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutw_label_78798";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtext_inputvalue_617031";
    item['en'] = "Input value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareafocus_textareacontent_498562";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextplaceholder_placeholder_98233";
    item['en'] = "Placeholder";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiouncheckeddisabled_radiolabel_11633";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutne_label_121445";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimarydisabled_buttonlabel_991181";
    item['en'] = "Button Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareaerror_textareacontent_642710";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselect_selectoption_102471";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiochecked_radiolabel_806458";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselectdisabled_selectoption_1027557";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextdisabled_inputvaluedisable_774043";
    item['en'] = "Input value (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimary_ok_678138";
    item['en'] = "OK";
    
    item = {};
    this.items.push(item);
    item['key'] = "formlegend_formelements_928871";
    item['en'] = "Form Elements";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondarydisabled_cancel_153306";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutsw_label_529181";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextfocus_inputvalue_977526";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextarea_textareacontent_716903";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutnw_label_422384";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallouts_label_111270";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloute_label_452344";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondary_cancel_1038778";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxunchecked_checklabel_51914";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "n12_mybank_776101";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n12_username_438114";
    item['en'] = "Username";
    
    item = {};
    this.items.push(item);
    item['key'] = "n12_password_744997";
    item['en'] = "Password";
    
    item = {};
    this.items.push(item);
    item['key'] = "n12_deutschebank_905754";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n12_login_294994";
    item['en'] = "Login";
    
    item = {};
    this.items.push(item);
    item['key'] = "n12_createnewaccount_165692";
    item['en'] = "Create new Account";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_mybank_503400";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_username_324122";
    item['en'] = "Username";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_password_185439";
    item['en'] = "Password";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_lastname_201258";
    item['en'] = "First name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_lastname2_759233";
    item['en'] = "Last name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_address_727081";
    item['en'] = "Address";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_email_570370";
    item['en'] = "Email";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_contact_717333";
    item['en'] = "Phone number";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_postal_384482";
    item['en'] = "Postal Code";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_pan_875298";
    item['en'] = "PAN";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_deutschebank_293166";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_signup_189719";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_mybank_618346";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_fields_213830";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_value_381462";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_fields2_468072";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_value2_959014";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_fields3_328604";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_value3_262466";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_fields4_457841";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_value4_152106";
    item['en'] = "Anmol Varma\nanmol.varma@gmail.com\n+91 7010223334\nABCDEF1235\n6th floor, Nalapad Brigade Centre\n632014";
    
    item = {};
    this.items.push(item);
    item['key'] = "n32_hianmol_977303";
    item['en'] = "Hi Anmol";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp1_textblock_195426";
    item['en'] = "My Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp2_textblock_960804";
    item['en'] = "Create new account";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp2_textblock_732200";
    item['en'] = "Login";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp2_textblock_404095";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_textblock_215892";
    item['en'] = "Login";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_textblock_304863";
    item['en'] = "Create new account";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_textblock_925326";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp1_textblock_996845";
    item['en'] = "Create new account";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp1_textblock_529453";
    item['en'] = "Login";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp1_textblock_520386";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp2_deutschebank_148910";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxchecked_checklabel_471575";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtexterror_inputvalue_249940";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareadisabled_textareacontentd_732451";
    item['en'] = "Text area content (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutn_label_176923";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiocheckeddisabled_radiolabel_536423";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxcheckeddisabled_checklabel_330718";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallout_label_736470";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiounchecked_radiolabel_126647";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxuncheckeddisabled_checklabel_989979";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutse_label_993001";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutw_label_283375";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtext_inputvalue_391810";
    item['en'] = "Input value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareafocus_textareacontent_709934";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextplaceholder_placeholder_500009";
    item['en'] = "Placeholder";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiouncheckeddisabled_radiolabel_80455";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutne_label_540106";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimarydisabled_buttonlabel_859759";
    item['en'] = "Button Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareaerror_textareacontent_680287";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselect_selectoption_452222";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiochecked_radiolabel_636028";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselectdisabled_selectoption_226191";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextdisabled_inputvaluedisable_513335";
    item['en'] = "Input value (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimary_ok_874120";
    item['en'] = "OK";
    
    item = {};
    this.items.push(item);
    item['key'] = "formlegend_formelements_553062";
    item['en'] = "Form Elements";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondarydisabled_cancel_987837";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutsw_label_282869";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextfocus_inputvalue_541558";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextarea_textareacontent_251656";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutnw_label_170233";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallouts_label_452507";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloute_label_921855";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondary_cancel_566016";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxunchecked_checklabel_230463";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_mybank_1012440";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_username_600815";
    item['en'] = "Username";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_password_528227";
    item['en'] = "Password";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_lastname_268769";
    item['en'] = "First name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_lastname2_256736";
    item['en'] = "Last name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_address_994806";
    item['en'] = "Address";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_email_1021148";
    item['en'] = "Email";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_contact_773909";
    item['en'] = "Phone number";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_postal_179465";
    item['en'] = "Postal Code";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_pan_815072";
    item['en'] = "PAN";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_deutschebank_696250";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_signup_987564";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxchecked_checklabel_62976";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtexterror_inputvalue_224822";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareadisabled_textareacontentd_479462";
    item['en'] = "Text area content (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutn_label_321143";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiocheckeddisabled_radiolabel_360348";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxcheckeddisabled_checklabel_649491";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallout_label_451149";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiounchecked_radiolabel_153809";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxuncheckeddisabled_checklabel_730451";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutse_label_91240";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutw_label_395666";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtext_inputvalue_514400";
    item['en'] = "Input value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareafocus_textareacontent_424814";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextplaceholder_placeholder_595216";
    item['en'] = "Placeholder";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiouncheckeddisabled_radiolabel_519774";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutne_label_458884";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimarydisabled_buttonlabel_896174";
    item['en'] = "Button Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareaerror_textareacontent_635378";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselect_selectoption_518348";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiochecked_radiolabel_508093";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselectdisabled_selectoption_891021";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextdisabled_inputvaluedisable_681716";
    item['en'] = "Input value (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimary_ok_738109";
    item['en'] = "OK";
    
    item = {};
    this.items.push(item);
    item['key'] = "formlegend_formelements_497656";
    item['en'] = "Form Elements";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondarydisabled_cancel_310719";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutsw_label_618423";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextfocus_inputvalue_25838";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextarea_textareacontent_720884";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutnw_label_525398";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallouts_label_327139";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloute_label_546873";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondary_cancel_869833";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxunchecked_checklabel_685845";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_mybank_249086";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_username_443929";
    item['en'] = "Username";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_password_774909";
    item['en'] = "Password";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_lastname_74961";
    item['en'] = "First name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_lastname2_315790";
    item['en'] = "Last name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_address_581443";
    item['en'] = "Address";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_email_245840";
    item['en'] = "Email";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_contact_138532";
    item['en'] = "Phone number";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_postal_561408";
    item['en'] = "Postal Code";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_pan_423966";
    item['en'] = "PAN";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_deutschebank_101820";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n23_signup_970623";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxchecked_checklabel_383080";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtexterror_inputvalue_185075";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareadisabled_textareacontentd_918425";
    item['en'] = "Text area content (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutn_label_472617";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiocheckeddisabled_radiolabel_909754";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxcheckeddisabled_checklabel_382284";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallout_label_926048";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiounchecked_radiolabel_1007336";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxuncheckeddisabled_checklabel_994455";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutse_label_887517";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutw_label_626243";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtext_inputvalue_360841";
    item['en'] = "Input value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareafocus_textareacontent_248543";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextplaceholder_placeholder_286767";
    item['en'] = "Placeholder";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiouncheckeddisabled_radiolabel_60836";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutne_label_36689";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimarydisabled_buttonlabel_495755";
    item['en'] = "Button Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextareaerror_textareacontent_381714";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselect_selectoption_112408";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "radiochecked_radiolabel_188093";
    item['en'] = "Radio Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "selectselectdisabled_selectoption_363948";
    item['en'] = "Select Option";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextdisabled_inputvaluedisable_813564";
    item['en'] = "Input value (Disabled)";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonsprimary_ok_383812";
    item['en'] = "OK";
    
    item = {};
    this.items.push(item);
    item['key'] = "formlegend_formelements_300481";
    item['en'] = "Form Elements";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondarydisabled_cancel_746159";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutsw_label_601418";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextfocus_inputvalue_917592";
    item['en'] = "Input Value";
    
    item = {};
    this.items.push(item);
    item['key'] = "inputtextarea_textareacontent_844723";
    item['en'] = "Text area content";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloutnw_label_340293";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescallouts_label_1045966";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "notescalloute_label_982388";
    item['en'] = "A";
    
    item = {};
    this.items.push(item);
    item['key'] = "buttonssecondary_cancel_273128";
    item['en'] = "Cancel";
    
    item = {};
    this.items.push(item);
    item['key'] = "checkboxunchecked_checklabel_659465";
    item['en'] = "Checkbox Label";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_mybank_12447";
    item['en'] = "MyBank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_username_633195";
    item['en'] = "Username";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_password_578283";
    item['en'] = "Password";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_lastname_21226";
    item['en'] = "First name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_lastname2_148058";
    item['en'] = "Last name";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_address_997623";
    item['en'] = "Address";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_email_426332";
    item['en'] = "Email";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_contact_447557";
    item['en'] = "Phone number";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_postal_798383";
    item['en'] = "Postal Code";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_pan_371467";
    item['en'] = "PAN";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_deutschebank_845271";
    item['en'] = "Deutsche Bank";
    
    item = {};
    this.items.push(item);
    item['key'] = "n22_signup_326410";
    item['en'] = "Signup";
    
    item = {};
    this.items.push(item);
    item['key'] = "userdetails_textblock_355117";
    item['en'] = "Name";
    
    item = {};
    this.items.push(item);
    item['key'] = "userdetails_fields_599735";
    item['en'] = "Name:\nEmail:\nContact:\nPAN:\nAddress:\n\nPostal Code:";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_text_574226";
    item['en'] = "New text. Double-click to edit";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_text_907008";
    item['en'] = "Email";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_text_557538";
    item['en'] = "Name";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_text_357864";
    item['en'] = "PAN";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_text_979904";
    item['en'] = "Contact";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_textcopy3_26499";
    item['en'] = "Postal Code";
    
    item = {};
    this.items.push(item);
    item['key'] = "comp3_textcopy3_750601";
    item['en'] = "Address";
    
    item = {};
    this.items.push(item);
    item['key'] = "header_button_577502";
    item['en'] = "Logout";
    
    item = {};
    this.items.push(item);
    item['key'] = "header_textblock2_907649";
    item['en'] = " ";
    
    item = {};
    this.items.push(item);
    item['key'] = "header_text_766811";
    item['en'] = "Logout";
    
    item = {};
    this.items.push(item);
    item['key'] = "header_textblock_595092";
    item['en'] = "My Bank";
  }

  getStringsByLanguage = () => {
    let stringsByLang = {};
    for (let row of this.items) {
      const locKey = row.key;
      for (let key in row) {
        if (key === 'key')
          continue;
        let langObj = stringsByLang[key] || {};
        langObj[locKey] = row[key];
        stringsByLang[key] = langObj;
      }
    }
    return stringsByLang;
  }

}
