import React, { Component } from 'react';
import './App.css';


export default class InputField extends Component {

  // Properties used by this component:
  // visualStateIndex

  constructor(props) {
    super(props);
    
    this.state = {
      rectangleCopy: '',
      rectangleCopy2: '',
      rectangle4: '',
      rectangleCopy3: '',
      rectangleCopy4: '',
    };
  }

  // --- Functions for component state index 0 (1 of 5) --- 
  
  textInputChanged_rectangleCopy = (event) => {
    this.setState({rectangleCopy: event.target.value});
  }
  
  renderState0() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangleCopy = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    
    return (
      <div className="InputField" style={baseStyle}>
        <div className="compContent">
          <input className='baseFont state0_elRectangleCopy' style={style_rectangleCopy} type="text" placeholder={this.props.locStrings.inputfield_rectangle4_636964} onChange={this.props.setValue} defaultValue={this.props.defaultValue}  />
        </div>
      </div>
    )
  }
  
  // --- Functions for component state index 1 (2 of 5) --- 
  
  textInputChanged_rectangleCopy2 = (event) => {
    this.setState({rectangleCopy2: event.target.value});
  }
  
  renderState1() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangleCopy2 = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    
    return (
      <div className="InputField" style={baseStyle}>
        <div className="compContent">
          <input className='baseFont state1_elRectangleCopy2' style={style_rectangleCopy2} type="text" placeholder={this.props.locStrings.inputfield_rectangle4_330175} onChange={this.props.setValue} defaultValue={this.props.defaultValue}  />
        </div>
      </div>
    )
  }
  
  // --- Functions for component state index 2 (3 of 5) --- 
  
  textInputChanged_rectangle4 = (event) => {
    this.setState({rectangle4: event.target.value});
  }
  
  renderState2() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangle4 = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    
    return (
      <div className="InputField" style={baseStyle}>
        <div className="compContent">
          <input className='baseFont state2_elRectangle4' style={style_rectangle4} type="password" placeholder={this.props.locStrings.comp3_rectangle4_313156} onChange={this.props.setValue} defaultValue={this.props.defaultValue}  />
        </div>
      </div>
    )
  }
  
  // --- Functions for component state index 3 (4 of 5) --- 
  
  textInputChanged_rectangleCopy3 = (event) => {
    this.setState({rectangleCopy3: event.target.value});
  }
  
  renderState3() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangleCopy3 = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    
    return (
      <div className="InputField" style={baseStyle}>
        <div className="compContent">
          <input className='baseFont state3_elRectangleCopy3' style={style_rectangleCopy3} type="email" placeholder={this.props.locStrings.inputfield_rectangle4_327283} onChange={this.props.setValue} defaultValue={this.props.defaultValue}  />
        </div>
      </div>
    )
  }
  
  // --- Functions for component state index 4 (5 of 5) --- 
  
  textInputChanged_rectangleCopy4 = (event) => {
    this.setState({rectangleCopy4: event.target.value});
  }
  
  renderState4() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    
    const style_rectangleCopy4 = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    
    return (
      <div className="InputField" style={baseStyle}>
        <div className="compContent">
          <input className='baseFont state4_elRectangleCopy4' style={style_rectangleCopy4} type="tel" placeholder={this.props.locStrings.inputfield_rectanglecopy3_400722} onChange={this.props.setValue} defaultValue={this.props.defaultValue}  />
        </div>
      </div>
    )
  }
  
  
  render() {
    switch (parseInt((this.state && this.state.visualStateIndexOverride !== undefined) ? this.state.visualStateIndexOverride : this.props.visualStateIndex, 10)) {
      default:
      case 0:
        return this.renderState0();
      case 1:
        return this.renderState1();
      case 2:
        return this.renderState2();
      case 3:
        return this.renderState3();
      case 4:
        return this.renderState4();
    }
  }
  

}
