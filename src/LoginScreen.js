import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import Button from './Button';
import Logo from './Logo';
import axios from 'axios';

export default class LoginScreen extends Component {

  // Properties used by this component:
  // appActions, deviceInfo, ds_username, ds_password

  constructor(props) {
    super(props);
    
    this.state = {
      usernameField: '',
      passwordField: '',
    };
  }

  onClick_elComp2 = (ev) => {
    // Go to screen 'Signup'
    this.props.appActions.goToScreen('signup', { transitionId: 'fadeIn' });
  
  }
  

  
  onClick_elComp3 = (ev) => {
      axios.post('http://35.240.214.157:8080/api/authenticate', {
        username: this.state.usernameField,
        password: this.state.passwordField
      }).then((response) => {
          window.localStorage.setItem('token',response.data.id_token);
          this.props.appActions.goToScreen('userdetail', { transitionId: 'fadeIn' });
        })
        .catch((error) => {
          alert('Invalid Credentials');
        });
  }
  
  
  textInputChanged_usernameField = (event) => {
    this.setState({usernameField: event.target.value});
  }
  
  textInputChanged_passwordField = (event) => {
    this.setState({passwordField: event.target.value});
  }
  
  render() {
    // eslint-disable-next-line no-unused-vars
    let baseStyle = {};
    // eslint-disable-next-line no-unused-vars
    let layoutFlowStyle = {};
    if (this.props.transitionId && this.props.transitionId.length > 0 && this.props.atTopOfScreenStack && this.props.transitionForward) {
      baseStyle.animation = '0.25s ease-in-out '+this.props.transitionId;
    }
    if ( !this.props.atTopOfScreenStack) {
      layoutFlowStyle.height = '100vh';
      layoutFlowStyle.overflow = 'hidden';
    }
    
    const style_background = {
        width: '100%',
        height: '100%',
     };
    const style_background_outer = {
        backgroundColor: '#f6f6f6',
        pointerEvents: 'none',
     };
    const style_comp2_outer = {
        cursor: 'pointer',
     };
    const style_rectangle = {
        background: 'rgba(241, 241, 241, 1.000)',
        border: '0.4px solid rgba(150, 150, 150, 0.4000)',
        display: 'none',
        pointerEvents: 'none',
     };
    const style_usernameField = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    const style_username = {
        fontSize: 17.2,
        color: '#00285f',
        textAlign: 'left',
        pointerEvents: 'none',
     };
    const style_passwordField = {
        display: 'block',
        backgroundColor: 'white',
        paddingLeft: '1rem',
        boxSizing: 'border-box', // ensures padding won't expand element's outer size
     };
    const style_password = {
        fontSize: 17.2,
        color: '#00285f',
        textAlign: 'left',
        pointerEvents: 'none',
     };
    
    return (
      <div className="AppScreen LoginScreen" style={baseStyle}>
        <div className="background">
          <div className='appBg containerMinHeight elBackground' style={style_background_outer}>
            <div style={style_background} />
          
          </div>
          
        </div>
        <div className="layoutFlow" style={layoutFlowStyle}>
          <div className='hasNestedComps elComp'>
            <div>
              <Header appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>
          
          </div>
          
          <div className='hasNestedComps elComp2' style={style_comp2_outer}>
            <div onClick={this.onClick_elComp2} >
              <Button appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>
          
          </div>
          
          <div className='hasNestedComps elComp3' style={style_comp2_outer}>
            <div onClick={this.onClick_elComp3} >
              <Button visualStateIndex={1} appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>
          
          </div>
          
        </div>
        <div className="screenFgContainer">
          <div className="foreground">
            <div className='elRectangle' style={style_rectangle} />
            <input className='baseFont elUsernameField' style={style_usernameField} type="text" placeholder={this.props.locStrings.n12_usernamefield_395840} onChange={this.textInputChanged_usernameField} defaultValue={this.state.usernameField}  />
            <div className='font-arialUnicodeMS  elUsername' style={style_username}>
              <div>{this.props.locStrings.n12_username_438114}</div>
            </div>
            <input className='baseFont elPasswordField' style={style_passwordField} type="password" placeholder={this.props.locStrings.n12_passwordfield_301802} onChange={this.textInputChanged_passwordField} defaultValue={this.state.passwordField}  />
            <div className='font-arialUnicodeMS  elPassword' style={style_password}>
              <div>{this.props.locStrings.n12_password_744997}</div>
            </div>
            <div className='hasNestedComps elComp4'>
              <Logo appActions={this.props.appActions} deviceInfo={this.props.deviceInfo} locStrings={this.props.locStrings} />
            </div>
          </div>
        </div>
      </div>
    )
  }
  

}
